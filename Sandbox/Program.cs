﻿using System;
using System.Threading.Tasks;
using Volumio.RestSdk;

namespace Sandbox
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var client = new VolumioClient("http://10.17.2.146");
            await client.PlayAsync();
            var playState = await client.GetStateAsync();
            await Task.Delay(2000);
            await client.PauseAsync();
            var pauseState = await client.GetStateAsync();
            await Task.Delay(2000);
            await client.StopAsync();
            var stopState = await client.GetStateAsync();
        }
    }
}
