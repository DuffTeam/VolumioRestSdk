﻿using Newtonsoft.Json;

namespace Volumio.RestSdk.Model
{
    public class CommandResponse
    {
        [JsonProperty("time")]
        public long Timestamp { get; set; }

        [JsonProperty("response")]
        public string Response { get; set; }

        [JsonProperty("Error")]
        public string Error { get; set; }
    }
}
