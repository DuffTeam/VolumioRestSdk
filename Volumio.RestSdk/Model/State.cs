﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Volumio.RestSdk.Model
{
    public class State
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public VolumioStatus Status { get; set; }

        public string Title { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public string AlbumArt { get; set; }
        public string Uri { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public TrackType TrackType { get; set; }

        public int? Seek { get; set; }
        public int? Duration { get; set; }
        public int? Channels { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public bool? Random { get; set; }
        public bool? Repeat { get; set; }
        public bool? RepeatSingle { get; set; }
        public bool? Consume { get; set; }
        public int? Volume { get; set; }
        public bool? Mute { get; set; }
        public bool? UpdateDb { get; set; }
        public bool? Volatile { get; set; }
        public string Service { get; set; }
    }

    public enum TrackType
    {
        [EnumMember(Value = "webradio")] WebRadio
    }

    public enum VolumioStatus
    {
        [EnumMember(Value = "play")] Play,
        [EnumMember(Value = "stop")] Stop,
        [EnumMember(Value = "pause")] Pause
    }
}
