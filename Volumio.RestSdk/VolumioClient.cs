﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Volumio.RestSdk.Model;

namespace Volumio.RestSdk
{
    public class VolumioClient
    {
        private readonly HttpClient _client;

        public VolumioClient(string address)
        {
            _client = new HttpClient {BaseAddress = new Uri(address)};
        }

        public async Task<bool> PlayAsync() => await SendCommandAsync("play");
        public async Task<bool> PlayAsync(int index) => await SendCommandAsync("play", "N", index.ToString());
        public async Task<bool> StopAsync() => await SendCommandAsync("stop");
        public async Task<bool> PauseAsync() => await SendCommandAsync("pause");
        public async Task<bool> PreviousAsync() => await SendCommandAsync("prev");
        public async Task<bool> NextAsync() => await SendCommandAsync("next");
        public async Task<bool> SetVolumeAsync(int value)
        {
            if (value < 0 || value > 100)
                throw new ArgumentOutOfRangeException(nameof(value), "Value must be between 0 and 100");
            return await SendCommandAsync("volume", "volume", value.ToString());
        }

        public async Task<bool> MuteAsync() => await SendCommandAsync("volume", "volume", "mute");
        public async Task<bool> UnmuteAsync() => await SendCommandAsync("volume", "volume", "unmute");
        public async Task<bool> VolumeUpAsync() => await SendCommandAsync("volume", "volume", "plus");
        public async Task<bool> VolumeDownAsync() => await SendCommandAsync("volume", "volume", "minus");
        public async Task<bool> ClearQueueAsync() => await SendCommandAsync("clearQueue");
        public async Task<bool> PlayPlaylistAsync(string name) => await SendCommandAsync("playplaylist", "name", name);


        public async Task<State> GetStateAsync() => await GetAsync<State>("/api/v1/getState");

        private async Task<T> GetAsync<T>(string requestUri) where T : class, new()
        {
            try
            {
                var response = await _client.GetStringAsync(requestUri);
                Debug.WriteLine(response);
                var payload = JsonConvert.DeserializeObject<T>(response);
                return payload;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private Task<bool> SendCommandAsync(string command, string paramName, string value)
        {
            var requestUri = $"/api/v1/commands/?cmd={command}&{paramName}={value}";
            return SendCommandAsyncImpl(command, requestUri);
        }

        private Task<bool> SendCommandAsync(string command, IDictionary<string, string> param = null)
        {
            var requestUri = $"/api/v1/commands/?cmd={command}";

            if (param != null)
            {
                var sb = new StringBuilder(requestUri);
                var pairs = param.Select(pair => $"&{pair.Key}={pair.Value}");
                foreach (var pair in pairs)
                    sb.Append(pair);
                requestUri = sb.ToString();
            }

            return SendCommandAsyncImpl(command, requestUri);
        }

        private async Task<bool> SendCommandAsyncImpl(string command, string requestUri)
        {
            try
            {
                var response = await _client.GetStringAsync(requestUri);
                var payload = JsonConvert.DeserializeObject<CommandResponse>(response);

                return payload?.Response == $"{command} Success";
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
